<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'markup'
    ];


    /**
     * The blocks that belong to the template.
     */
    public function blocks()
    {
        return $this->belongsToMany('App\Block');
    }
}
