<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class FileController extends Controller
{
    public function index() {
        return view('files.index', ['files' => File::get()]);
    }
}
