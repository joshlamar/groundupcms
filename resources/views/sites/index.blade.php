<h1>Sites</h1>
<ol>
    @foreach($sites as $site)
        <li>{{ $site->name }}
            <ol>
                @foreach($site->teams as $team)
                    <li>{{ $team->name }}</li>
                @endforeach
            </ol>
        </li>
    @endforeach
</ol>