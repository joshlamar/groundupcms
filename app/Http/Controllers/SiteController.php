<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Site;

class SiteController extends Controller
{
    public function index() {
        return view('sites.index', ['sites' => Site::with('teams')->get()]);
    }
}
