<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'path', 'url', 'type', 'size'
    ];


    /**
     * The field that the file belongs to
     */
    public function fields()
    {
        return $this->belongsToMany('App\Field');
    }
}
