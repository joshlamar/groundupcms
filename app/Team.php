<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    /**
     * The users that belong to the team.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * The sites that this team belongs to.
     */
    public function sites()
    {
        return $this->belongsToMany('App\Site');
    }
}
