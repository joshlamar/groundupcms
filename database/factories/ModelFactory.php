<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Block::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
    ];
});

$factory->define(App\Field::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
        'type' => $faker->randomElement(['text', 'date', 'number', 'checkbox', 'radio', 'select', 'email']),
        'placeholder' => $faker->sentence(3),
    ];
});

$factory->define(App\File::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
        'url' => $faker->imageUrl(1280, 960),
        'size' => $faker->randomNumber(5),
        'type' => $faker->mimeType,
    ];
});

$factory->define(App\Form::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
    ];
});

$factory->define(App\Page::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
    ];
});


$factory->define(App\Site::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
        'url' => $faker->url,
        'logo' => $faker->imageUrl(200,100),
    ];
});


$factory->define(App\Team::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
    ];
});

$factory->define(App\Template::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(3),
        'markup' => $faker->randomHtml(2,3),
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});