<h1>Templates</h1>
<ol>
    @foreach($templates as $template)
        <li>{{ $template->name }}
            <ol>
                @foreach($template->blocks as $block)
                    <li>{{ $block->name }}</li>
                @endforeach
            </ol>
        </li>
    @endforeach
</ol>