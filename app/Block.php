<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The templates that belong to the block.
     */
    public function templates()
    {
        return $this->belongsToMany('App\Template');
    }
}
