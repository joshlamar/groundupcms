<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    // Teams
    Route::get('/teams', 'TeamApiController@index');
    Route::get('/teams/{id}', 'TeamApiController@show');
    Route::post('/teams', 'TeamApiController@store');
    Route::put('/teams/{id}', 'TeamApiController@update');
    Route::delete('/teams/{id}', 'TeamApiController@destroy');
});
