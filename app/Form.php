<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    /**
     * The pages this form belongs to
     */
    public function pages()
    {
        return $this->belongsToMany('App\Page');
    }

    /**
     * The first page and likely only page
     */
    public function page()
    {
        return $this->pages()->first();
    }

    /**
     * The form has many fields
     */
    public function fields()
    {
        return $this->hasMany('App\Field');
    }
}
