<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class TeamApiController extends Controller
{
    public function index() {
        return Team::with('users')->get();
    }

    public function store(Request $request) {
        return Team::create($request->all());
    }

    public function destroy($id) {
        Team::findOrFail($id)->delete();
        return "deleted";
    }
}
