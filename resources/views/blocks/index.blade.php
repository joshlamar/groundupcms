<h1>Blocks</h1>
<ol>
    @foreach($blocks as $block)
        <li>{{ $block->name }}
            <ol>
                @foreach($block->templates as $template)
                    <li>{{ $template->name }}</li>
                @endforeach
            </ol>
        </li>
    @endforeach
</ol>