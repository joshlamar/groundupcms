export const ADD_TEAM = 'ADD_TEAM'
export const LOAD_TEAMS = 'LOAD_TEAMS'
export const DELETE_TEAM = 'DELETE_TEAM'
export const REMOVE_USER = 'REMOVE_USER'