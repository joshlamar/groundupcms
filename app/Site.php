<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The teams that belong to the site.
     */
    public function teams()
    {
        return $this->belongsToMany('App\Team');
    }
}
