import * as types from '../mutation-types'

// initial state
const state = {
    all: []
}

// getters
const getters = {
    allTeams: state => state.all
}

// actions
const actions = {
    addTeam ({ commit, state }, name) {
        axios.post('/api/teams', {name: name}).then( commit(types.ADD_TEAM, {name: name}) )
    },

    deleteTeam ({ commit, state }, team) {
        axios.delete('/api/teams/' + team.id).then( commit(types.DELETE_TEAM, team) )
    },

    loadTeams({ commit, state}) {
        axios.get('/api/teams').then( response => commit(types.LOAD_TEAMS, response.data) )
    }
}

// mutations
const mutations = {

    [types.ADD_TEAM] (state, team) {
        state.all.push(team)
    },

    [types.DELETE_TEAM] (state, team) {
        state.all.map((t, i) => {
            if(t.id == team.id) {
                state.all.splice(i, 1)
            }
        })
    },

    [types.LOAD_TEAMS] (state, teams) {
        state.all = teams
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}