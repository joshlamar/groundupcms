@extends('layouts.app')

@section('content')
    <h1>Teams</h1>
    <create-team-form></create-team-form>
    <team-list :teams="{{ $teams }}"></team-list>
@endsection
