<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'default', 'placeholder', 'validation', 'form_id'
    ];


    /**
     * The form that owns this field
     */
    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
