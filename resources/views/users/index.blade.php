<h1>Users</h1>
<ol>
    @foreach($users as $user)
        <li>{{ $user->name }}
            <ol>
                @foreach($user->teams as $team)
                    <li>{{ $team->name }}</li>
                @endforeach
            </ol>
        </li>
    @endforeach
</ol>