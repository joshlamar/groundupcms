<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;

class TemplateController extends Controller
{
    public function index() {
        return view('templates.index', ['templates' => Template::with('blocks')->get()]);
    }
}
