<h1>Pages</h1>
<ol>
    @foreach($pages as $page)
        <li>{{ $page->name }} ({{ $page->site ? $page->site->name : "no way" }})
        </li>
    @endforeach
</ol>