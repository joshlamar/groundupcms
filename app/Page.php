<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'site_id', 'template_id'
    ];

    /**
     * The sites this page belongs to
     */
    public function site()
    {
        return $this->belongsTo('App\Site');
    }

    /**
     * The sites this page belongs to
     */
    public function template()
    {
        return $this->hasOne('App\Template');
    }
}
