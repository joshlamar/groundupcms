<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BlocksTableSeeder::class);
        $this->call(FieldsTableSeeder::class);
        $this->call(FilesTableSeeder::class);
        $this->call(FormsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(SitesTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(TemplatesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        foreach(App\User::with('teams')->get() as $model) {
            $model->teams()->attach(App\Team::inRandomOrder()->first()->id);
        }

        foreach(App\Site::with('teams')->get() as $model) {
            $model->teams()->attach(App\Team::inRandomOrder()->first()->id);
        }

        foreach(App\Page::with('site')->get() as $model) {
            $model->site()->associate(App\Site::inRandomOrder()->first());
            $model->save();
        }

        foreach(App\Field::with('form')->get() as $model) {
            $model->form()->associate(App\Form::inRandomOrder()->first());
            $model->save();
        }

        foreach(App\Block::with('templates')->get() as $model) {
            $model->templates()->attach(App\Template::inRandomOrder()->first()->id);
        }


    }
}
