<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;

class FormController extends Controller
{
    public function index() {
        return view('forms.index', ['forms' => Form::with('fields')->get()]);
    }
}
