<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function index() {
        return view('pages.index', ['pages' => Page::with('site')->get()]);
    }
}
